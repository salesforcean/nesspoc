public class NES_ITRUtils {
    private static Peak_ApexLog logger = new Peak_ApexLog('EnrollmentHelper');
    
    // Created by Maddileti for US #335371 on 12/04/2020
    // Creating New PEs ,Opportunity & ECA's for New School Year
    /*public static Peak_Response nextYearEnrollment(Id studentId,String instituteNam,String schoolYear,String gradeLevels,String callType, boolean enrollFlag,Id programEnrollmentId, String enrollmentType) {
        Peak_Response peakResponse = new Peak_Response();
        NES_ECACompletion.shouldRunCompletion = false;
        NES_Constants.runAssignment = false;
        NES_LTSUtility.runLTSFlag = false;
        NES_ProgramEnrollmentHandler.runCBL = false;
        
        if(studentId !=null){
            
            User caretaker = New User();
            if(callType=='community'){
                caretaker= NES_ReactivateStdUtils.getCTUser(UserInfo.getUserId());
            } else if(callType=='application'){
                caretaker= NES_ReactivateStdUtils.getCTUserFromStudentId(studentId);
            } else {
                System.debug('No callType');
                return peakResponse;
            }
            Id studentRecordType = NES_ReactivateStdUtils.getStudentRTId();
            contact reactivateStudent = NES_ReactivateStdUtils.getStudentContact(studentId,studentRecordType);
            School_Year__c schoolYears = NES_ReactivateStdUtils.getSchoolYears(schoolYear);
            Account instituteName = NES_ReactivateStdUtils.getInstitute(instituteNam);
            
            try{
                
                //List<NES_EvaluateUtil.Question> questionDataList = NES_AddStudentHelper.grabQuestionsBySchool(instituteName.id,schoolYears.id);
                //Get Program Enrollments and insert them.  Also retrieve the field that determines which version of the ECA 
                Id academicProgramRecordTypeId = NES_ReactivateStdUtils.getAcademicRTProgramId();
                Account academicProgram = [
                    SELECT Id, name,ParentId, RecordTypeId, School_Year__c, OwnerId, Confirmation_End_Date__c, Use_Updated_ECA_Process__c,
                    Too_Young_Cutoff__c, Too_Old_Cutoff__c,MaxSpecialEdBirthdate__c
                    FROM Account
                    WHERE parent.name =:instituteNam
                    AND RecordTypeId = :academicProgramRecordTypeId
                    AND School_Year__c= :schoolYears.id
                    LIMIT 1
                ];
                
                List<hed__Program_Enrollment__c> caretakerEnrollments = [SELECT Id, hed__Contact__c FROM hed__Program_Enrollment__c WHERE hed__Contact__c = :caretaker.ContactId AND hed__Account__c = :academicProgram.Id limit 1];
                // Grade_Level__c grades=[SELECT id,Name FROM Grade_Level__c  where Name =:gradeLevels limit 1];
                
                hed__Program_Enrollment__c caretakerEnrollmentId = new hed__Program_Enrollment__c();
                if (caretakerEnrollments.size() == 0) {
                    Id caretakerEnrollmentTypeId = Schema.SObjectType.hed__Program_Enrollment__c.getRecordTypeInfosByName().get('Caretaker').getRecordTypeId();
                    caretakerEnrollmentId.hed__Contact__c = caretaker.ContactId;
                    caretakerEnrollmentId.RecordTypeId = caretakerEnrollmentTypeId;
                    caretakerEnrollmentId.OwnerId = academicProgram.OwnerId;
                    if (academicProgram != null) {
                        caretakerEnrollmentId.hed__Account__c = academicProgram.Id;
                    }
                    insert caretakerEnrollmentId;
                } else {
                    caretakerEnrollmentId = caretakerEnrollments[0];
                }
                
                
                Program_Grade_Level__c progGrades =[select id,Grade_Level__r.name from Program_Grade_Level__c where Academic_Program__r.name =:academicProgram.name AND Grade_Level__r.name =:gradeLevels limit 1];
                //Program Enrollment for student
                hed__Program_Enrollment__c newStudentEnrollment = new hed__Program_Enrollment__c();
                Id studentEnrollmentTypeId = Schema.SObjectType.hed__Program_Enrollment__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
                newStudentEnrollment.hed__Contact__c = reactivateStudent.Id;     
                newStudentEnrollment.RecordTypeId = studentEnrollmentTypeId;
                newStudentEnrollment.Household_Program_Enrollment__c = caretakerEnrollmentId.id;
                newStudentEnrollment.hed__Account__c = academicProgram.Id;
                newStudentEnrollment.Start_Year__c = schoolYears.Id;
                newStudentEnrollment.OwnerId = academicProgram.OwnerId;
                newStudentEnrollment.Program_Grade_Level__c  = progGrades.id;
                newStudentEnrollment.Final_Grade_Level__c  = progGrades.Grade_Level__r.name;
                newStudentEnrollment.Has_Open_Opportunity__c = true;
                system.debug('enrollFlag=='+enrollFlag);
                // add by Maddileti for US # 332934 on 12/16/2020 
                if(enrollFlag){
                  
                    newStudentEnrollment.Enrollment_Type__c = 'Re-enrolling student';
                    
                } else {
                    
                    hed__Program_Enrollment__c oldPE =[select Enrollment_Type__c from hed__Program_Enrollment__c where id =:programEnrollmentId limit 1];
                    newStudentEnrollment.Enrollment_Type__c =oldPE.Enrollment_Type__c;
                    
                }   
                // End by Maddileti for US # 332934 on 12/16/2020 
                string str = Id.valueOf(reactivateStudent.id); 
                
                try{
                    
                    insert newStudentEnrollment;
                }
                catch(Exception e){
                    logger.logException('Program Enrollment insertion failed for Contact :'+str, e);
                    logger.saveLogs();                        
                }
                
                //Now let's create an Opportunity based on that enrollmentId
                //Modified the ownerId as per the bug #77521 to match the Academic Program Owner
                
                Opportunity oppToInsert = new Opportunity(
                    Name = reactivateStudent.FirstName + ' ' + reactivateStudent.LastName + ' Opportunity',
                    OwnerId = academicProgram.OwnerId,
                    Caretaker_Id__c = caretaker.ContactId,
                    Student_Id__c = reactivateStudent.Id,
                    AccountId = reactivateStudent.AccountId,
                    Program_Enrollment__c = newStudentEnrollment.Id,
                    StageName = 'Open',
                    CloseDate = academicProgram.Confirmation_End_Date__c == null ? Date.today() : academicProgram.Confirmation_End_Date__c.date()
                );
                
                
                string strs = Id.valueOf(reactivateStudent.id); 
                
                try{
                    
                    insert oppToInsert;
                }
                catch(Exception e){
                    logger.logException('Opportunity insertion failed for Contact :'+strs, e);
                    logger.saveLogs();                        
                }
                
                // instantiate affiliations to insert, gather record type ids
                Set<Enrollment_Component_Affiliation__c> ecAffiliationsToInsert = new Set<Enrollment_Component_Affiliation__c>();
                List<Enrollment_Component__c> processComponent = NES_ReactivateStdUtils.getProcessEC(academicProgram.Id);
                List<Enrollment_Component_Relationship__c> relationships  = NES_ReactivateStdUtils.getAllStages(processComponent[0].Id);
                
                
                
                //Hang on to the birth date so we can use it in the hard stop logic at the end of the process.
                date birthdate;
                
                List<Enrollment_Component_Affiliation__c> basicECAsforStage = NES_ReactivateStdUtils.insertAffiliations(processComponent[0].Id, newStudentEnrollment.Id,relationships);
                ecAffiliationsToInsert.addAll(basicECAsforStage);
                
                List<Enrollment_Component_Affiliation__c> allECAs =new List<Enrollment_Component_Affiliation__c>();
                allECAs.addAll(ecAffiliationsToInsert);
                
                integer i=0;
                do{
                    try{
                        i++;
                        insert allECAs;                
                        break;//Breaking the loop as the DML executed and no exceptions
                    }
                    catch(Exception e){
                        logger.logException('Retrying a timed out insert', e);
                        logger.saveLogs();                        
                    }
                }while(i<5);
                
                //Determine which version of the ECA process we are using.
                if (academicProgram.Use_Updated_ECA_Process__c == false)
                {
                    //Call an @future that will create the rest of the ECAs
                    List<Id> relationIds = new List<Id>();
                    for (Enrollment_Component_Relationship__c r :relationships)
                        relationIds.add(r.id);
                    
                    NES_ECAEvaluation.createRemainingAffiliations(caretaker.Contact.AccountId, reactivateStudent.Id, caretaker.ContactId, newStudentEnrollment.Id, caretakerEnrollmentId.id, relationIds, processComponent[0].Id);
                } else  {  //Must be using the new ECA process
                    //Call the future method that will create the remaining ECAs and the PE Criteria.
                    EvaluationUtils.PerformAsyncNewStudentSteps(newStudentEnrollment.Id, processComponent[0].Id, caretakerEnrollmentId.id);
                }
                
                //With the ECA rewrite, we changed the hard stop process to only look at dates from the Academic program.
                Boolean shouldHardStop = false;
                //Replaced with MaxSpecialEdBirthdate__c instead of Too_Old_Cutoff__c in below if condition for 335863
                
                birthdate = reactivateStudent.Birthdate__c;
                System.debug('birth date:'+birthdate);
                if (birthdate < academicProgram.MaxSpecialEdBirthdate__c || birthdate > academicProgram.Too_Young_Cutoff__c)
                    shouldHardStop = true;
                
                if (shouldHardStop) {
                    String message = label.NES_Add_Student_Hard_Stop_Message;
                    peakResponse.messages.add(message);
                    newStudentEnrollment.Status__c = 'Ineligible';
                    update newStudentEnrollment; 
                    
                    //added for the US 169322 - Jagadeesh Bokam
                    //If the student has been hard stopped, the opp should be closed lost.
                    oppToInsert.StageName = 'Closed Lost';
                    oppToInsert.CloseDate = Date.today();
                    update oppToInsert;
                    //Ended for the US 169322 - Jagadeesh Bokam
                } 
                
                //The case creation is now seperated out to a queable.
                System.enqueueJob(new NES_AddStudentCaseQueueable(caretaker,newStudentEnrollment.Id,reactivateStudent));
                
                
            }catch(exception e){
                
                peakResponse.success = false;
                peakResponse.messages.add(e.getMessage());
                System.debug('Exception thrown: ' + e.getMessage());
                System.debug(e.getMessage());
                System.debug(e.getStackTraceString());
                logger.logException('insertingpes', e);
                logger.saveLogs();
                
            }
        } 
        
        
        
        return peakResponse;
    }
    
   public static void createPEforITR(contact student, Account academicProgram, hed__Program_Enrollment__c caretakerEnrollmentId, School_Year__c schoolYears,String nextGradeLevel){       
        Program_Grade_Level__c progGrades =[select id,Grade_Level__r.name from Program_Grade_Level__c where Academic_Program__r.name =:academicProgram.name AND Grade_Level__r.name =:nextGradeLevel limit 1];
        //Program Enrollment for student
        hed__Program_Enrollment__c newStudentEnrollment = new hed__Program_Enrollment__c();
        Id studentEnrollmentTypeId = Schema.SObjectType.hed__Program_Enrollment__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
        newStudentEnrollment.hed__Contact__c = student.Id;     
        newStudentEnrollment.RecordTypeId = studentEnrollmentTypeId;
        newStudentEnrollment.Household_Program_Enrollment__c = caretakerEnrollmentId.id;
        newStudentEnrollment.hed__Account__c = academicProgram.Id;
        newStudentEnrollment.Start_Year__c = schoolYears.Id;
        newStudentEnrollment.OwnerId = academicProgram.OwnerId;
        newStudentEnrollment.Program_Grade_Level__c  = progGrades.id;
        newStudentEnrollment.Final_Grade_Level__c  = progGrades.Grade_Level__r.name;
        newStudentEnrollment.Has_Open_Opportunity__c = true;
		newStudentEnrollment.Enrollment_Type__c = 'Returning Student';
		insert newStudentEnrollment; 
    }
    
    @TestVisible private Opportunity getNewOpportunity(hed__Program_Enrollment__c newStudentEnrollment, contact student, Account academicProgram, user caretaker) { 
		Opportunity oppToInsert = new Opportunity(
                    Name = student.FirstName + ' ' + student.LastName + ' Opportunity',
                    OwnerId = academicProgram.OwnerId,
                    Caretaker_Id__c = caretaker.ContactId,
                    Student_Id__c = student.Id,
                    AccountId = student.AccountId,
                    Program_Enrollment__c = newStudentEnrollment.Id,
                    StageName = 'Open',
                    CloseDate = academicProgram.Confirmation_End_Date__c == null ? Date.today() : academicProgram.Confirmation_End_Date__c.date()
                );
       
		insert oppToInsert;
        return oppToInsert;
    }
        
    
    public static void createITRECAs(List<hed__Program_Enrollment__c> peRecord){
        
        
        //Retrive process EC with academic program
        List<Enrollment_Component__c> processECRec = [Select Id,Name,
                                                      RecordType.Name,
                                                      Process_Academic_Program__c
                                                      from Enrollment_Component__c where Process_Academic_Program__c=:peRecord[0].hed__Account__c and RecordType.Name =: NES_DashboardUtilities.PROCESS_RT];
        
        
        NES_EnrollmentCompentBuilder allECData = NES_EnrollmentCompentBuilder.getInstance();
        NES_EnrollmentCompentBuilder.enrollmentComponentData thisProcessData = allECData.getSpecificProcessECData(processECRec[0].Id);
        List<Enrollment_Component_Affiliation__c> newAffiliations = new List<Enrollment_Component_Affiliation__c>();  
        
        //Inserting Process ECA
        Enrollment_Component_Affiliation__c processECA = new Enrollment_Component_Affiliation__c();
        processECA.Enrollment_Component__c = processECRec[0].Id;
        processECA.Program_Enrollment__c = peRecord[0].Id;
        processECA.Status__c = 'Not Started';
        processECA.EC_Record_Type__c = NES_DashboardUtilities.PROCESS_RT;   
        newAffiliations.add(processECA);
        
        //Add Stage ECA records
        for (Enrollment_Component_Relationship__c relationship : thisProcessData.stageRelationships) {  
            System.debug('Needed for Reenroll****'+relationship.Child_Enrollment_Component__r.Needed_for_ITR__c);             
            if(relationship.Child_Enrollment_Component__r.Needed_for_ITR__c == true && String.isBlank(relationship.Child_Enrollment_Component__r.Assignment_Logic__c)) {
                Enrollment_Component_Affiliation__c affiliationRecord = new Enrollment_Component_Affiliation__c();
                affiliationRecord.Enrollment_Component__c = relationship.Child_Enrollment_Component__c;
                affiliationRecord.Program_Enrollment__c = peRecord[0].Id;
                affiliationRecord.Status__c = 'Not Started';
                affiliationRecord.Order__c = relationship.Order__c;
                affiliationRecord.EC_Record_Type__c = relationship.Child_Enrollment_Component__r.RecordType.Name;   
                newAffiliations.add(affiliationRecord);                   
            }                    
        }
        
        //Add form ECA records
        for (Enrollment_Component_Relationship__c relationship : thisProcessData.formRelationships) { 
            System.debug('Needed for Reenroll****'+relationship.Child_Enrollment_Component__r.Needed_for_ITR__c);              
            if(relationship.Child_Enrollment_Component__r.Needed_for_ITR__c == true  && String.isBlank(relationship.Child_Enrollment_Component__r.Assignment_Logic__c))  {
                Enrollment_Component_Affiliation__c affiliationRecord = new Enrollment_Component_Affiliation__c();
                affiliationRecord.Enrollment_Component__c = relationship.Child_Enrollment_Component__c;
                affiliationRecord.Program_Enrollment__c = peRecord[0].Id;
                affiliationRecord.Status__c = 'Not Started';
                affiliationRecord.Order__c = relationship.Order__c;
                affiliationRecord.EC_Record_Type__c = relationship.Child_Enrollment_Component__r.RecordType.Name;   
                newAffiliations.add(affiliationRecord);                    
            }                 
        }
        
        //Add Section ECA records
        for (Enrollment_Component_Relationship__c relationship : thisProcessData.sectionRelationships) {  
            System.debug('Needed for Reenroll****'+relationship.Child_Enrollment_Component__r.Needed_for_ITR__c);             
            if(relationship.Child_Enrollment_Component__r.Needed_for_ITR__c == true &&  String.isBlank(relationship.Child_Enrollment_Component__r.Assignment_Logic__c) )  {
                Enrollment_Component_Affiliation__c affiliationRecord = new Enrollment_Component_Affiliation__c();
                affiliationRecord.Enrollment_Component__c = relationship.Child_Enrollment_Component__c;
                affiliationRecord.Program_Enrollment__c = peRecord[0].Id;
                affiliationRecord.Status__c = 'Not Started';
                affiliationRecord.Order__c = relationship.Order__c;
                affiliationRecord.EC_Record_Type__c = relationship.Child_Enrollment_Component__r.RecordType.Name;   
                newAffiliations.add(affiliationRecord);                
            }                 
        }
        
        System.debug('newAffiliations**********'+newAffiliations);
        if (newAffiliations.size() > 0) {
            insert newAffiliations; 
        }
        //Calling assignment logic to create ECAs to create schooling Info section
        //NES_ECAEvaluation.runAssignment(peRecord[0].Id, null, peRecord[0],newAffiliations,processECRec[0].Id);
        EvaluationUtils.PerformAsyncNewStudentSteps(peRecord[0].Id, processECRec[0].Id, peRecord[0].Household_Program_Enrollment__c);
    }*/
    
}